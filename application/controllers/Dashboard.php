<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {


	 public  $result_array = array() ;



	public $session_data = array();

	public function __construct()
	{
		parent::__construct();		

		$this->session_data = $this->session->userdata;

		$this->load->model('Chartmodel','chart');

		$auth_settings['sessionId'] = @$this->session->userdata('devicewise_response')['sessionId'];
		$auth_settings['_endPoint'] = DEVICE_WISE_API;
		$auth_settings['userName'] = $this->session->userdata('uname');
		$auth_settings['userPass'] = $this->session->userdata('pass');

		$this->result_array  = $this->chart->segregate_things($auth_settings);

	}


	

	public function index()
	{

			$this->map();
	}


	public function solid_gauge(){


			$data = array();

			$get = $this->input->get();

			$data = array_merge($data, $get);

			$this->view_data['menu_active'] = 'dashboard';

			$this->view_data['view_file'] = 'pages/solid-gauge';

			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/js/solid-gauge.js"></script>'.PHP_EOL;

			$this->load->view('index', $this->view_data);

	}


	public function temperature(){

		
			$data = array();

			$get = $this->input->get();

			$data = array_merge($data, $get);


			$this->view_data['menu_active'] = 'temperature';

			$this->view_data['view_file'] = 'pages/temperature';

			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/js/temperature.js"></script>'.PHP_EOL;


			$this->load->view('index', $this->view_data);
	}


	public function map(){

			
			$data = array();

			$get = $this->input->get();

			$groups = array();


			foreach ($this->result_array['things_result'] as $thing_info) {

				if(isset($thing_info['tags'])  &&  count($thing_info['tags']) > 0){

					foreach ($thing_info['tags'] as $group_name) {

						$groups[] = $group_name;

					}
				}	

			}


			$groups = array_unique($groups);

			asort($groups);
			
			$data['thing_result'] = $this->result_array;

			$data['groups'] 	  = $groups;


			$this->view_data['data'] = array_merge($data, $get);

			$this->view_data['menu_active'] = 'map';

			$this->view_data['view_file'] = 'pages/map';


			$this->view_data['js_file'] = '<script type="text/javascript" src="assets/js/map.js"></script>'.PHP_EOL;
			


			$this->load->view('index', $this->view_data);
	}



	

}
