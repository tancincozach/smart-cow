<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	 		

	public $result_array = array();
	
	public function __construct(){

		parent::__construct();


	/*
		WEOLCAN CREDENTIALS
		Username: weolcan@iotzone.co
		Password: S1MenormalX72.

	;*/

		$sess = $this->session->userdata;

	 	if(array_key_exists('sessionId', $sess['devicewise_response'])){

				$this->load->model('Chartmodel','chart');

 				$auth_settings['sessionId'] = $sess['devicewise_response']['sessionId'];	
				$auth_settings['_endPoint'] = DEVICE_WISE_API;
							
				$this->result_array  = $this->chart->segregate_things($auth_settings);


	 	} 


		$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
		strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
		if(!$isAjax) {

		    header('HTTP/1.0 404 Not Found');
		    echo "<h1>404 Not Found</h1>";
		    echo "The page that you have requested could not be found.";		  
		  
		  	exit();
		}	



		if( !$this->session->userdata('logged_in')){
				
				$cookie = get_cookie('sesshpr');
				$cookie = base64_decode($cookie);
				$cookie = json_decode($cookie);

				if(isset($cookie->logged_in)){


					$sess['logged_in'] 	= 1; 
					$sess['devicewise_response'] 	=  $cookie['devicewise_response']['auth']['params'];
					$sess['uname'] 	= $cookie['uname'];
					$sess['pass'] 	= $cookie['pass'];

					
			
					$this->session->set_userdata($sess);
				}

		}
	}
	

	public function get_things(){
		

			if( count($this->result_array) > 0 ){

				if(count($this->result_array) == 1){

						$json = json_encode($this->result_array[0]);
				}else{
						$json = json_encode($this->result_array);
				}

				echo $json;
			}
		
	}

	public function implement_configuration(){

		try {


				$this->load->helper('file');

				$post = $this->input->post();

				$return = array('error'=>0);

				$settings  =  json_decode( $this->load->view('html-includes/config.json','',true),TRUE);
			
				foreach ($post as $key => $value) {
							
							$settings[$key] = $value;
				}

				if(count($settings) == 0 ) throw new Exception("Empty configuration");

				$json_output = json_encode($settings);

				if ( ! write_file(APPPATH.'views/html-includes/config.json', $json_output)) throw new Exception("Unable to write config");
				
				echo json_encode(array_merge(array('msg'=>'Configuration succesfully implemented.'),array('config'=>$settings)));				
			
		} catch (Exception $e) {

				echo json_encode(array('error'=>$e->getMessage()));	
			
		}


		

	}


	public function get_config(){

				$this->load->helper('file');

				$post = $this->input->post();

				$return = array('error'=>0);

				$json_output  = $this->load->view('html-includes/config.json','',true);
						

				echo $json_output;

	}


}
