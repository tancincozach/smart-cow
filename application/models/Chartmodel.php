<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




class Chartmodel extends CI_Model{ 




	/*
		  a method that retrieves the coordinates for the geoference or for the google map api.
		*/
		public function getDeviceCoordinates( $thing_result) {


				$geo_location_info = array();

				foreach ($thing_result as $thing) {

					  
						    $address = array();

						  if(isset($thing['loc']['addr']['streetNumber']) ){
						    array_push($address,$thing['loc']['addr']['streetNumber']);
						  }

						  if(isset($thing['loc']['addr']['street']) ){
						    array_push($address,$thing['loc']['addr']['street']);
						  }

						  if(isset($thing['loc']['addr']['city']) ){
						    array_push($address,$thing['loc']['addr']['city']);
						  } 

						  if(isset($thing['loc']['addr']['state']) ){
						    array_push($address,$thing['loc']['addr']['state']);
						  } 

					 	$geo_location_info [] = array (
													'fulladdress'=> (count($address) > 0 ) ? implode(" ",$address) :"Geolocation is not available.",
													'lat'=>  @$thing['loc']['lat'],
													'lng'=> @$thing['loc']['lng']     
							);

				}

				return $geo_location_info;
		  
		}

		public function segregate_things( $auth_settings){


			 $things_array = array();

			$this->load->library('Devicewise' ,$auth_settings );			


			$this->devicewise->session_org_switch(DEVICE_WISE_ORG);		

			$ctr=0;

			foreach($this->devicewise->thing_list() as $parent_key=> $thing_array){


					$things_array[]= $this->devicewise->thing_find($thing_array['key']);
					
					
					

					$thing_def_array  = $this->devicewise->thing_def_find($thing_array['defKey']);  

					$thing_def_properties = @$thing_def_array['properties'];

					
					unset($thing_def_array);

					 if(isset($thing_def_properties))
					  {
					    if(count($thing_def_properties) > 0 )
					    {      
					       foreach($thing_def_properties as $propKey=>$property)
					       {


					         if(array_key_exists($propKey, $thing_def_properties))
					         {          
					            $prefix = isset($thing_def_properties[$propKey]['prefix']) ? $thing_def_properties[$propKey]['prefix']:$thing_def_properties[$propKey]['name'];


					            if(isset($things_array[$ctr]['properties']) && array_key_exists($propKey, $things_array[$ctr]['properties'])){


					            	$things_array[$ctr]['properties'][$propKey] = array_merge( $things_array[$ctr]['properties'][$propKey], $thing_def_properties[$propKey]);
					            
					            }
			        			
					         }          
					       }
					      
					    }
					  }
								
				  
	 		  	if(isset($things_array[$ctr]['lastSeen'])){		

	 		  			$things_array[$ctr]['lastSeenRaw'] = $things_array[$ctr]['lastSeen']; 

						$datetime = date('Y-m-d H:i:s',strtotime($things_array[$ctr]['lastSeen']));

						$nz_tz = new DateTime($datetime, new DateTimeZone("Pacific/Auckland"));

						$things_array[$ctr]['lastSeen'] =  $nz_tz->format("Y-m-d H:i:s"); 

				  } 
				  
	 		  	if(isset($things_array[$ctr]['locUpdated'])){


	 		  			$things_array[$ctr]['locUpdatedRaw'] = $things_array[$ctr]['locUpdated']; 

						$datetime = date('Y-m-d H:i:s',strtotime($things_array[$ctr]['locUpdated']));

						$nz_tz = new DateTime($datetime, new DateTimeZone("Pacific/Auckland"));

						$things_array[$ctr]['locUpdated'] =  $nz_tz->format("Y-m-d H:i:s"); 

				  } 

			
					$ctr++;
			}

			$last_seen = $this->get_last_thing_seen($things_array,'locUpdated');

		 	return  array('things_result'=>$things_array , 'last_seen_thing'=> $things_array[$last_seen['id']]);
		 	
	}

	function sort($key) {
	    return function ($a, $b) use ($key) {
	        return strcmp(@$a[$key], @$b[$key]);
	    };
	}

	function get_last_thing_seen($array,$field_name) {

		   $found_value = array();

		   foreach ($array as $key => $value) {		   		

		   		if(isset($value[$field_name])){
		   			$found_value[] = array('id'=>$key,$field_name=>$value[$field_name]);		   					
		   			
		   		}
		   		
		   }
		
   		   usort($found_value, $this->sort($field_name));	

   		   return @$found_value[count($found_value)-1];
	   
	}

}