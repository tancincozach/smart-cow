<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rest {

        protected $CI;

    
      /**
   * The API endpoint for POSTing (e.g. https://www.example.com/api).
   * @var string
  * author TANCINCO ZACH 
   */

  public $_endPoint = '';

  /**
   * The application identifier you will be using.
   * @var string
   */

  public $appId = '';

  /**
   * This application appToken.
   * @var string
   */

  public $appToken = '';

  /**
   * The thing key used to identify the application.
   * @var string
   */

  public $apiKey = '';

  /**
   * The userName used to connect to the server.
   * @var string
   */
  public $userName = '';

  /**
   * The passWord used to connect to the server.
   * @var string
   */
  public $userPass = '';

  /**
   * Last JSON string received from the endpoint. Used when debugging.
   * @var string
   */

  public $lastReceived = '';

  /**
   * Last JSON string sent to the endpoint. Used when debugging.
   * @var string
   */

  public $lastSent = '';

  /**
   * If the last request succeeded or failed.
   * @var bool
   */

  public $lastStatus = null;

  /**
   * Holds the response data from the api call.
   * @var array
   */

  public $response = array();

  /**
   * Holds the current session identifier.
   * @var string
   */

  public $sessionId = '';

  /**
   * Holds any error returned by the api.
   * @var array
   */

  public $error = array();


  /**
   * Initialize the object.
   * @param array $options The initialization options.
   */




  public function __construct( $params = array() )
  {           



        $this->userName =  @$params['userName'];
        $this->userPass =  @$params['userPass'];
        $this->_endPoint =  @$params['_endPoint'];
        $this->sessionId = @$params['sessionId'];



      $this->CI =& get_instance();

    // if the sessionId is not set or if diag.ping fails, authenticate.

    //if (empty($this->sessionId) or !$this->exec('diag.ping')) {     
    if (empty($this->sessionId) ) {     


      $this->sessionId = '';
      $this->auth();
    }


  }


  /**
   * This method sends the TR50 request to the server and parses the response.
   * @param string $json The JSON command and arguments. This parameter can also be an array that will be converted to JSON.
   * @return boolean Success or failure to post.
   */

  public function post($json)
  {  


   $this->error = $this->lastStatus = $this->lastReceived = $this->response = '';



  $json = $this->set_json_auth($json).'<br/>';
    

    

    if (PHP_SAPI == 'cli') {
      $ip = '127.0.0.1';
    }
    else {
      $ip = (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    }

    $this->lastSent = $json;
   

      $curlOptions =    array(
                             CURLOPT_URL => $this->_endPoint,
                             CURLOPT_SSL_VERIFYPEER =>false,      
                             CURLOPT_HEADER=>false,   
                             CURLINFO_HEADER_OUT=>true,   
                             CURLOPT_RETURNTRANSFER =>true,
                             CURLOPT_POST => 1,
                             CURLOPT_HTTPHEADER =>array("X-Forwarded-For: {$ip}"),
                             CURLOPT_POSTFIELDS =>$json
                           );
 
    


        $ch = curl_init();





        curl_setopt_array($ch,$curlOptions);



        $response = curl_exec($ch);




  
        //Checking for cURL errors
        if (curl_errno($ch)) {          
           $this -> error = curl_error($ch);
           curl_close($ch);
           return false;
           //Handle errors
        } else  {
           curl_close($ch);
           $responseArray = array();
       

           parse_str($response,$responseArray);
       
        }        

    $this->lastReceived = $response;


    $this->response = json_decode($response, true);
     
      

    $this->error = (array_key_exists('errorMessages', $this->response)) ? $this->response['errorMessages'] : '';

    $this->lastStatus = (array_key_exists('success', $this->response)) ? $this->response['success'] : true;
    
    return array('status'=>$this->lastStatus , 'response'=>$this->response);
  }


  /**
   * Return the response data for the last command if the last command was successful.
   * @return array The response data.
   */

  public function get_response()
  {
    if ($this->lastStatus and !empty($this->response['data'])) {      
      return $this->response['data'];
    }
    return null;
  }


  /**
   * This method checks the JSON command for the auth parameter. If it is not set, it adds it.
   * @param mixed $json A JSON string or the array representation of JSON.
   * @return string A JSON string with the auth parameter.
   */

  public function set_json_auth($json)
  {
    $json = (!is_array($json)) ? json_decode($json, true) : $json;

    if (!isset($json['auth'])) {
      // if the sessionId is empty, authenticate
      if (empty($this->sessionId)) {
        $this->auth();
      }
      // if it is still empty, we cannot proceed
      if (empty($this->sessionId)) {
        trigger_error("Authorization failed. Please check the application configuration.", E_USER_ERROR);
      }
      $json['auth']['sessionId'] = $this->sessionId;
    }

    return json_encode($json);
  }


  /**
   * Package the command and the params into an array and sends the command to the configured endpoint for processing.
   * @param string $command The TR50 command to execute.
   * @param array $params The array of command parameters.
   * @return boolean Success or failure to post.
   */

  public function exec($command, $params = false)
  {

      if ($command == 'api.authenticate') {
      $array = array('auth' => array('command' => 'api.authenticate',
                                     'params'  => $params));   



    }
    else {
      
      $array = array('data' => array('command' => $command));

      if ($params !== false and is_array($params)) {
        $array['data']['params'] = (object)$params;
      }

    } 


    return $this->post($array);  
  }


  /**
   * Package the command and the params into an array and sends the command to the configured endpoint for processing.
   * @param string $command The TR50 command to execute.
   * @param array $params The array of command parameters.
   * @return boolean Success or failure to post.
   */

  /*public function exec($command, $params = false)
  {
    if ($command == 'api.authenticate') {
      $array = array('auth' => array('command' => 'api.authenticate',
                                     'params'  => $params));    
    }
    else {
      $array = array('data' => array('command' => $command));
      if ($params !== false and is_array($params)) {
        $array['data']['params'] = (object)$params;
      }
    }

    return $this->post($array);  
  }*/




  /**
   * Depending on the configuration, authenticate as an application or as a user. Prefer authentication as an application.
   * @return boolean Success or failure to authenticate.
   */

  public function auth()
  {
    /*echo '<pre>';
    print_r(get_object_vars($this));
    echo '<pre/>';
    exit;*/
    if (!empty($this->appId) and !empty($this->appToken) and !empty($this->apiKey)) {      
      return $this->app_auth($this->appId, $this->appToken, $this->apiKey);
    }
    elseif (!empty($this->userName) and !empty($this->userPass)) {
    
      return $this->user_auth($this->userName, $this->userPass);
    }
    return false;
  }


 /**
   * Depending on the configuration, authenticate as an application or as a user. Prefer authentication as an application.
   * @return boolean Success or failure to authenticate.
   */

  public function auth_login()
  {
    if (!empty($this->appId) and !empty($this->appToken) and !empty($this->apiKey)) {      
      return $this->app_auth($this->appId, $this->appToken, $this->apiKey);
    }
    elseif (!empty($this->userName) and !empty($this->userPass)) {
      return $this->devicewise_user_auth($this->userName, $this->userPass);
    }
    return false;
  }

  /**
   * Authenticate the application.
   * @param string $appId The application ID.
   * @param string $appToken The application token.
   * @param string $apiKey The key of the application's thing.
   * @param boolean $update_session_id Update the object session ID.
   * @return boolean Success or failure to authenticate.
   */

  public function app_auth($appId, $appToken, $apiKey, $update_session_id = true)
  {
    $params = array('appId'    => $appId,
                    'appToken' => $appToken,
                    'apiKey' => $apiKey);
    
    if ($this->exec('api.authenticate', $params)) {
      if ($update_session_id) {
        $this->sessionId = $this->response['auth']['params']['sessionId'];
      }
      return true;
    }

    return false;
  }


  /**
   * Authenticate a user.
   * @param string $userName The userName.
   * @param string $passWord The passWord.
   * @param boolean $update_session_id Update the object session ID.
   * @return boolean Success or failure to authenticate.
   */

  public function user_auth($username, $password, $update_session_id = true)
  {
    
    $params = array('username' => $username,
                    'password' => $password);


    if ($result = $this->exec('api.authenticate', $params)) {

      if ($update_session_id) {
        $this->sessionId = @$this->response['auth']['params']['sessionId'];
      }
      return true;
   } 

    return false;
  }

/**
   * Authenticate a user.
   * @param string $userName The userName.
   * @param string $passWord The passWord.
   * @param boolean $update_session_id Update the object session ID.
   * @return boolean Success or failure to authenticate.
   */

  public function devicewise_user_auth($username, $password)
  {


    
      $params = array('username' => $username,
                    'password' => $password);



      $result = $this->exec('api.authenticate', $params);


      if (isset($update_session_id)) {
        $this->sessionId = $this->response['auth']['params']['sessionId'];
      }

      return $result;

  }


  /**
   * Returns an array of the options set in the object. Useful for initializing a new object.
   * @return array The array of options.
   */

  public function get_options()
  {
    return array('endpoint'   => $this->endpoint,
                 'sessionId'  => $this->sessionId,
                 'appId'      => $this->appId,
                 'appToken'   => $this->appToken,
                 'apiKey'   => $this->apiKey,
                 'userName'   => $this->userName,
                 'userPass'   => $this->userPass);
  }


  /**
   * After a command is executed, this method returns an array of debugging information about the object and the last command.
   * @return array Debugging data.
   */

  public function debug()
  {
    return array('endpoint'     => $this->endpoint,
                 'lastSent'     => $this->lastSent,
                 'lastReceived' => $this->lastReceived,
                 'error'        => $this->error);
  }
}


?>