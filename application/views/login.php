<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Smart Cow Admin</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="assets/css/styles.css" rel="stylesheet">
		<link href="assets/css/signin.css" rel="stylesheet">
	</head>
	<body>
<!-- header -->
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid navbar-inner">
        <div class="navbar-header">
          
            <a class="navbar-brand" href="#">Smart Cow</a>
        </div>
   
    </div>
    <!-- /container -->
</div>
<!-- /Header -->

   <?php if($this->session->flashdata('fmesg'))
    {
        ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong><?php echo @$this->session->flashdata('fmesg');?></strong> 
        </div>
        <?php
    }

    if($this->session->flashdata('error'))
    {
        ?>
        <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> <?php echo @$this->session->flashdata('error');?></strong>
        </div>
        <?php
    }
 ?> 


<div class="account-container">
	
	<div class="content clearfix">

	<?php

		echo form_open('', '  accept-charset="utf-8"   role="form" name="login-form" method="post"'); 
	?>
		
		
			<h1>Login Page</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your Devicewise Credential details</p>
				
				<div class="field">
					<label for="username">Username</label>
					<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
								
									
				<button class="button btn btn-success btn-large">Sign In</button>
				
			</div> <!-- .actions -->
			
			
			
	<?php 

		echo form_close();

	?>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->
<!-- /Main -->

<?php $this->load->view('html-includes/footer')?>
<!-- /.modal -->
	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
	</body>
</html>

