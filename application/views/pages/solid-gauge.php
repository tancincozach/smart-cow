  <div class="row">
            

                <div class="col-sm-12">
                
                    <a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> Dashboard Statistics Overview</strong></a>
                    <hr>
                    
                </div>

      </div>

  
    <div class="row">

        <div class="col-xs-12 col-md-4 " >

          <div class="panel">
                    <div class=" panel-header-bg"><i class="glyphicon glyphicon-signal"></i>&nbsp;<label>Input Voltage VIN</label></div>
                    <div class="panel-content">
                          <div id="input-voltage" ></div>
                    </div>
            </div>

        </div>

          <div class="col-xs-12 col-md-4 " >

          <div class="panel">
                    <div class=" panel-header-bg"><i class="glyphicon glyphicon-signal"></i>&nbsp;<label> Battery Stack Voltage V(BAT)</label></div>
                    <div class="panel-content">
                                <div id="batt-stack-voltage"></div>
                    </div>  
            </div>

        </div>

        <div class="col-xs-12 col-md-4 " >

          <div class="panel">
                    <div class=" panel-header-bg"><i class="glyphicon glyphicon-signal"></i>&nbsp;<label> System Voltage V(SYS)</label></div>
                    <div class="panel-content">
                                <div id="system-voltage"></div>
                    </div>
            </div>

        </div>
       

                  
    </div>

      <div class="row">

         <div class="col-xs-12 col-md-4 " >

            <div class="panel">
                      <div class=" panel-header-bg"><i class="glyphicon glyphicon-signal"></i>&nbsp;<label>Input Voltage VIN</label></div>
                      <div class="panel-content">
                                  <div id="voltage-1"></div>
                      </div>
              </div>

          </div>
           <div class="col-xs-12 col-md-4 " >

            <div class="panel">
                      <div class=" panel-header-bg"><i class="glyphicon glyphicon-signal"></i>&nbsp;<label>Battery Stack Voltage V(BAT)</label></div>
                      <div class="panel-content">
                                  <div id="batt-current"></div>
                      </div>
              </div>

          </div>

           <div class="col-md-4">
             
          </div>
                    
      </div>