<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Smart Cow Admin</title>		

        <base href="<?php echo base_url(); ?>" />

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="assets/css/styles.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css" rel="stylesheet">        
        <link href="assets/plugins/map-icons-master/dist/css/map-icons.css" rel="stylesheet">                
        

        <?php echo @$css_file; ?>
	</head>
	<body>


<?php $this->load->view('html-includes/header')?>
<?php $this->load->view('html-includes/side-bar' , @$data);?>

<script type="text/javascript">
    var smart_cow_base_url = '<?php echo base_url(); ?>';

    var smart_cow_global = {
        n:'<?php echo $this->security->get_csrf_token_name(); ?>',
        h:'<?php echo $this->security->get_csrf_hash(); ?>'        
    } 
</script>



<!-- Use any element to open the sidenav -->
<a href="#"  class="open_pane" ><i class="glyphicon glyphicon-triangle-right
" style="font-size:40px;" title="Open"></i></a>
<!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
<div id="main">

<div id="system-msg"></div>

    <div class="container-fluid">


        <?php
        //$this->load->view('html-includes/top-menu' ,  @$data);?>

        <?php $this->load->view($view_file, @$data); ?>


    </div>


</div>
<!-- /Main -->


<?php $this->load->view('html-includes/footer',@$data);?>
    <!-- /.modal-dalog -->




<div class="modal" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Widget</h4>
            </div>
            <div class="modal-body">
                <p>Add a widget stuff here..</p>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>

<!-- /.modal -->
	<!-- script references -->


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/bootstrap-toggle-master/js/bootstrap2-toggle.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyC_7sWD-o0bYY8cQviHJFHDTrYxJpI5Nh8&sensor=true"> </script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="assets/plugins/map-icons-master/dist/js/map-icons.js"></script>
<script src="assets/js/scripts.js"></script>

    <?php echo @$js_file; ?>

	</body>
</html>