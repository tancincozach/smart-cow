      <div class="subnavbar">
                  <div class="subnavbar-inner">
                    <div class="container">
                      <ul class="mainnav">
                        <li  <?php echo (@$menu_active=='dashboard' ? 'class="active"':''); ?>><a href="dashboard/index"><i class="icon-dashboard"></i><span>Solid Gauge</span> </a> </li>
                        <li <?php echo (@$menu_active=='temperature' ? 'class="active"':''); ?>><a href="dashboard/temperature"><i class="icon-list-alt"></i><span>Temperature</span> </a> </li>
                        <li <?php echo (@$menu_active=='map' ? 'class="active"':''); ?>><a href="dashboard/map"><i class="icon-facetime-video"></i><span>Map</span> </a></li>
                      </ul>
                    </div>
                    <!-- /container --> 
                  </div>
                  <!-- /subnavbar-inner --> 
                </div>
