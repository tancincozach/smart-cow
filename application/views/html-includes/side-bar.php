
    <div id="mySidenav" class="sidenav">


        <div  id="config-msg" class="row" style="padding-top:20px">
            
        </div>

        <div class="row">
            <div class="col-md-12 side-nav-content" >
            &nbsp;
              <a href="#" class="pull-right close_pane" ><i class="glyphicon glyphicon-remove"  style="font-size:40px;"></i></a>                    
                <ul class="nav nav-stacked">
                    <li><input id="auto-load-switch" checked data-toggle="toggle" data-on="<i class='fa fa-play'></i> Play" data-off="<i class='fa fa-stop'></i> Stop" type="checkbox"  data-onstyle="success" data-offstyle="danger" data-size="mini" data-width="180"></li>
                    <li><input type="text" name="input_time" class="form-control" placeholder="Time Interval" value="0">
                    <select name="time_type" class="form-control" >
                            <option value="min">Minutes</option>
                            <option value="sec">Seconds</option>
                    </select></li>                        
                </ul>

                <ul class="nav nav-stacked">
                    <li><label>Filter :</label></li>
                    <li>                          
                           <?php 


                              if(count($groups) > 0):

                                foreach($groups as $value) :

                                     $group_color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                           ?>
                                   <div class="checkbox">
                                        <label><input type="checkbox" name="filter"  class="filter_group" alt="<?php echo  $group_color;?>" value="<?php echo $value?>" checked/><?php echo strtoupper($value);?> <span style="background:<?php echo $group_color;?>;padding:10px;float:right;margin-left:40px"></span></label>
                                    </div>
      
                            <?php
                                endforeach;
                              endif;
                            ?>
                        

                    </li>
                </ul>

            </div>
        </div>
    </div>